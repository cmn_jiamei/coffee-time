export const environment = {
  production: false,
  apiUrl: 'https://coffeetime-node.herokuapp.com/api'
};
