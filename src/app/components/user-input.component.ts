import {Component, EventEmitter, Output} from '@angular/core';
import {UserService} from './user.service';
import {User} from './user.model';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-user-input',
  templateUrl: './user-input.component.html',
  styleUrls: ['./user-input.component.css']
})

export class UserInputComponent {
  @Output()
  public onUserCreated = new EventEmitter();

  constructor(private userService: UserService) {
  }

  public onSubmit(form: NgForm) {
    const self = this;

    this.userService
      .postUserWithName(form.value.name)
      .subscribe(() => {
        self.onUserCreated.emit();
      });

    form.resetForm();
  }
}

