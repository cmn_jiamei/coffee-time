import {Component, OnInit} from '@angular/core';
import {CoffeeService} from './coffee.service';

@Component({
  selector: 'app-coffee',
  templateUrl: './coffee.component.html',
  styleUrls: ['./coffee.component.css']
})
export class CoffeeComponent implements OnInit {
  isOccupied = false;

  public statusData: object;

  constructor(private coffeeService: CoffeeService) {}

  ngOnInit() {
    // Immediately gets status and initialize the polling afterwards
    this.getStatus();
    this.startPollingForStatus();
  }

  private startPollingForStatus() {
    setInterval(() => {
      this.getStatus();
    }, 3000);
  }


  private getStatus() {
    const self = this;

    this.coffeeService.getStatus().subscribe((status) => {
      self.statusData = status['data'];
    });
  }
}

