import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../environments/environment.dev';

@Injectable()
export class CoffeeService {

  constructor(private http: HttpClient) {}

  // Uses http.get() to load data from a single API endpoint
  public getStatus() {
    return this.http.get(environment.apiUrl + '/status');
  }
}

