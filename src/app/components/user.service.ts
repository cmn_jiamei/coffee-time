import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment.dev';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {}

  public getUsers() {
    return this.http.get(environment.apiUrl + '/coffee-queue');
  }

  public postUserWithName(name: string) {
    const body = {name: name};

    return this.http.post(environment.apiUrl + '/coffee-queue', body);
  }
}

