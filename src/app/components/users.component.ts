import {Component, OnInit} from '@angular/core';
import {UserService} from './user.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public users: object[] = [];

  constructor(private userService: UserService) {}

  ngOnInit() {
    // Immediately gets status and initialize the polling afterwards
    this.getUsers();
    this.startPollingForUsers();
  }

  private startPollingForUsers() {
    setInterval(() => {
      this.getUsers();
    }, 3000);
  }

  private getUsers() {
    const self = this;

    this.userService.getUsers().subscribe((status) => {
      self.users = status['data'];
    });
  }

  public refresh() {
    this.getUsers();
  }

  public getRandomImage(): string {
    const images = [
      'assets/images/avatar_blue.svg',
      'assets/images/avatar_green.svg',
      'assets/images/avatar_red.svg',
      'assets/images/avatar_yellow.svg'
    ];

    return images[Math.floor(Math.random() * images.length)];;
  }
}
