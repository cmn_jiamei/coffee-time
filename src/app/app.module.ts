import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {UserComponent} from './components/user.component';
import {UsersComponent} from './components/users.component';
import {UserInputComponent} from './components/user-input.component';
import {MomentModule} from 'ngx-moment';
import {FormsModule} from '@angular/forms';
import {CoffeeComponent} from './components/coffee.component';
import {HttpClientModule} from '@angular/common/http';
import {TimeAgoPipe} from 'time-ago-pipe';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    UsersComponent,
    UserInputComponent,
    CoffeeComponent,
    TimeAgoPipe
  ],
  imports: [
    BrowserModule,
    MomentModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
