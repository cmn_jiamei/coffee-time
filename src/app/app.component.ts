import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {UserService} from './components/user.service';
import {CoffeeService} from './components/coffee.service';
import {UsersComponent} from './components/users.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService, CoffeeService]
})
export class AppComponent {
  @ViewChild(UsersComponent)
  public usersComponent: UsersComponent;

  public userWasCreated() {
    this.usersComponent.refresh();
  }


}
